package me.mathiaseklund.anarchy;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.objects.Profile;

public class EnderChestCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			Profile profile = main.getProfileManager().getProfile(player);
			if (profile.isPremium()) {
				player.openInventory(player.getEnderChest());
			}
		}
		return false;
	}

}
