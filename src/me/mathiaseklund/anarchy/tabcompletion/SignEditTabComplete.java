package me.mathiaseklund.anarchy.tabcompletion;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class SignEditTabComplete implements TabCompleter {

	@Override
	public List<String> onTabComplete(CommandSender sender, Command arg1, String arg2, String[] args) {
		List<String> list = new ArrayList<String>();
		if (args.length == 1) {
			list.add("1");
			list.add("2");
			list.add("3");
			list.add("4");
		} else if (args.length == 2) {
			list.add("String");
		}
		return list;
	}

}
