package me.mathiaseklund.anarchy.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.anarchy.Main;

public class InventoryListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		if (main.getShopManager().isInShop(player)) {
			main.getShopManager().closedShop(player);
		} else if (main.getBountyManager().isInMenu(player)) {
			main.getBountyManager().closedMenu(player);
		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		if (main.getShopManager().isInShop(player)) {
			event.setCancelled(true);
			int slot = event.getRawSlot();
			if (slot >= 0 && slot < 45) {
				if (event.getClick() == ClickType.RIGHT) {
					ItemStack is = event.getCurrentItem();
					if (is != null) {
						main.getShopManager().attemptPurchase(is, player);
					}
				}
			} else if (slot >= 45 && slot < 54) {
				if (slot == 45) {
					main.getShopManager().openPreviousPage(player);
				} else if (slot == 53) {
					main.getShopManager().openNextPage(player);
				}
			}
		} else if (main.getBountyManager().isInMenu(player)) {
			event.setCancelled(true);
			int slot = event.getRawSlot();
			if (slot >= 0 && slot < 45) {
				// do nothing
			} else if (slot >= 45 && slot < 54) {
				if (slot == 45) {
					main.getBountyManager().openPreviousPage(player);
				} else if (slot == 53) {
					main.getBountyManager().openNextPage(player);
				}
			}
		}
	}
}
