package me.mathiaseklund.anarchy.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;

public class VoteListener implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler
	public void onVote(VotifierEvent event) {
		Vote vote = event.getVote();
		String username = vote.getUsername();
		Player player = Bukkit.getPlayer(username);
		if (player != null) {
			Profile prof = main.getProfileManager().getProfile(player);
			int lvls = 1;
			if (prof.isPremium()) {
				lvls = 2;
			}
			player.setLevel(player.getLevel() + lvls);
			util.message(player, "&aThank your for voting!&e You've been rewarded with " + lvls + " Level(s).");
		} else {
			main.getProfileManager().queueVoteRewards(username);
		}
	}
}
