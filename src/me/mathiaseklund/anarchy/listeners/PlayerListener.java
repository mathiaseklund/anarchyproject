package me.mathiaseklund.anarchy.listeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.Ageable;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
//import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class PlayerListener implements Listener {

	Main main = Main.getMain();

	HashMap<String, Long> timer = new HashMap<String, Long>();
	HashMap<String, Double> totalEarned = new HashMap<String, Double>();
	ArrayList<String> etimer = new ArrayList<String>();
	HashMap<String, Double> startedAt = new HashMap<String, Double>();
	HashMap<String, Sign> editingSign = new HashMap<String, Sign>();

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		Player killer = player.getKiller();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (killer != null) {
					if (main.getBountyManager().hasBounty(player)) {
						main.getBountyManager().redeemBounty(killer, player);
					}
				}
				main.getProfileManager().getProfile(player).resetBalance();
			}
		});

	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (main.isResetting()) {
			event.setResult(Result.KICK_OTHER);
			event.setKickMessage(ChatColor.translateAlternateColorCodes('&',
					"&eServer is currently being reset. Please join back momentarily."));
		} else {
			Player player = event.getPlayer();
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					main.getProfileManager().loadProfile(player);
				}
			});
		}
	}

	@EventHandler
	public void onBlockDamage(BlockDamageEvent event) {
		Player player = event.getPlayer();
		timer.put(player.getName(), Calendar.getInstance().getTimeInMillis());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			Profile prof = main.getProfileManager().getProfile(player);
			if (player.getGameMode() == GameMode.SURVIVAL) {
				ItemStack tool = player.getInventory().getItemInMainHand();
				if (tool.getType().toString().contains("_AXE") || tool.getType().toString().contains("_PICKAXE")
						|| tool.getType().toString().contains("_SHOVEL") || tool.getType().toString().contains("_SWORD")
						|| tool.getType().toString().contains("_HOE")) {
					// do nothing
				} else {
					tool = player.getInventory().getItemInOffHand();
				}
				if (event.getBlock().getDrops(tool) != null || !event.getBlock().getDrops(tool).isEmpty()) {
					Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
						public void run() {
							long now = Calendar.getInstance().getTimeInMillis();
							long then = 0;
							if (timer.containsKey(player.getName())) {
								then = timer.get(player.getName());
							}
							timer.remove(player.getName());
							long diff = now - then;
							double earned = diff / 1000.0;
							if (then == 0) {
								earned = 0.01;
							}
							earned = (double) Math.round(earned * 100) / 100;
							double total = 0.00;
							if (totalEarned.containsKey(player.getName())) {
								total = totalEarned.get(player.getName());
							}
							totalEarned.put(player.getName(), (double) Math.round((total + earned) * 100) / 100);
							// sendEarnedMessage(player);
							main.getProfileManager().getProfile(player).addBalance(earned);
							Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
								public void run() {
									player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent
											.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&cBalance:&6 $"
													+ main.getProfileManager().getProfile(player).getBalance())));
								}
							}, 1);

						}
					});
				}

				Block block = event.getBlock();
				if (prof.isPremium()) {
					Material type = block.getType();
					if (type == Material.WHEAT || type == Material.BEETROOTS || type == Material.CARROTS
							|| type == Material.POTATOES || type == Material.SWEET_BERRY_BUSH) {
						if (tool.getType().toString().contains("_HOE")) {
							Bukkit.getScheduler().runTaskLater(main, new Runnable() {
								public void run() {
									event.getBlock().setType(type);
								}
							}, 1);
						}
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak_hoe(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Profile profile = main.getProfileManager().getProfile(player);
		if (profile.isPremium()) {
			Block block = event.getBlock();
			Material type = block.getType();
			ItemStack is = player.getInventory().getItemInMainHand();
			if (is != null) {
				if (is.getType().toString().contains("_HOE")) {
					if (type == Material.WHEAT || type == Material.BEETROOTS || type == Material.CARROTS
							|| type == Material.POTATOES || type == Material.SWEET_BERRY_BUSH) {
						// if (block.getType() == Material.WHEAT) {
						Ageable age = (Ageable) block.getBlockData();
						if (age.getAge() < age.getMaximumAge()) {
							event.setCancelled(true);
						}
						// }
					}
				}
			}
		}
	}

	void sendEarnedMessage(Player player) {
		if (etimer.contains(player.getName())) {
			// do nothing
		} else {
			etimer.add(player.getName());
			startedAt.put(player.getName(), main.getProfileManager().getProfile(player).getBalance());
			Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
				public void run() {
					etimer.remove(player.getName());
					double earned = main.getProfileManager().getProfile(player).getBalance()
							- startedAt.get(player.getName());
					startedAt.remove(player.getName());
					totalEarned.put(player.getName(), 0.00);
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eMoney Earned: &6" + (earned)));
				}
			}, 10 * 20);
		}
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		Profile profile = main.getProfileManager().getProfile(player);
		if (profile.isPremium()) {
			event.setFormat(ChatColor.translateAlternateColorCodes('&', "&6[P]&r ") + event.getFormat());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerBedEnter(PlayerBedEnterEvent event) {
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			Profile prof = main.getProfileManager().getProfile(player);
			if (prof.isPremium()) {
				player.getWorld().setTime(0);
			}

		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onInteract(PlayerInteractEvent event) {
		if (event.useInteractedBlock() != org.bukkit.event.Event.Result.DENY) {
			// Player player = event.getPlayer();
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block block = event.getClickedBlock();
				if (block.getType().toString().contains("_SIGN")) {
					// Sign sign = (Sign) block.getState();
					// open(player, sign);
				}
			}
		}
	}

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		Player player = event.getPlayer();
		Profile prof = main.getProfileManager().getProfile(player);
		if (prof.isPremium()) {
			List<String> lines = new ArrayList<String>();
			for (String s : event.getLines()) {
				lines.add(ChatColor.translateAlternateColorCodes('&', s));
			}

			for (int i = 0; i < 4; i++) {
				event.setLine(i, lines.get(i));
			}
		}
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Location loc = event.getRespawnLocation();
		Location bedLoc = event.getPlayer().getBedSpawnLocation();
		if (bedLoc == null) {
			int x, z;
			boolean negative = false;
			Random random = new Random();
			int r = random.nextInt(2);
			if (r > 0) {
				negative = true;
			}
			if (negative) {
				x = -random.nextInt(1000);
				z = -random.nextInt(1000);
			} else {
				x = random.nextInt(1000);
				z = random.nextInt(1000);
			}

			loc.add(x, 0, z);

			Block b = loc.getWorld().getHighestBlockAt(loc);
			loc = b.getLocation();

			event.setRespawnLocation(loc);
		}

		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				Player p = event.getPlayer();
				if (main.isBot(p.getName())) {
					ItemStack is = new ItemStack(Material.NETHERITE_PICKAXE);
					ItemMeta im = is.getItemMeta();
					im.addEnchant(Enchantment.VANISHING_CURSE, 1, true);
					im.setUnbreakable(true);
					is.setItemMeta(im);
					ItemStack bread = new ItemStack(Material.BREAD, 128);
					Bukkit.getScheduler().runTask(main, new Runnable() {
						public void run() {
							p.getInventory().addItem(is);
							p.getInventory().addItem(bread);
						}
					});

				}
			}
		}, 1);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (timer.containsKey(player.getName())) {
			timer.remove(player.getName());
		}
	}

}
