package me.mathiaseklund.anarchy.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import me.mathiaseklund.anarchy.Main;
import net.md_5.bungee.api.ChatColor;

public class ServerListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onServerListPing(ServerListPingEvent event) {
		event.setMotd(ChatColor.translateAlternateColorCodes('&', main.getMotd()));
	}
}
