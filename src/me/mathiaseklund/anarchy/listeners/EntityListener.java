package me.mathiaseklund.anarchy.listeners;

import java.util.Calendar;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class EntityListener implements Listener {

	Main main = Main.getMain();

	HashMap<String, BossBar> bars = new HashMap<String, BossBar>();
	HashMap<String, Long> lasthit = new HashMap<String, Long>();

	private String barFormat = ChatColor.translateAlternateColorCodes('&', "&a%hp%&7/&c%max%");

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!event.isCancelled()) {
			if (event.getDamager() instanceof Player) {
				Player player = (Player) event.getDamager();
				LivingEntity ent = (LivingEntity) event.getEntity();
				double hp = ent.getHealth();
				double damage = event.getFinalDamage();
				double prog = 0;
				double max = ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
				hp = (double) Math.round((hp - damage) * 100) / 100;
				if (hp < 0) {
					hp = 0;
				} else {

					prog = (hp / max);
				}
				BossBar bar = main.getBarManager().getBossBar(player.getName());
				bar.setTitle(barFormat.replace("%hp%", hp + "").replace("%max%", max + ""));
				bar.setProgress(prog);
				if (!bar.getPlayers().contains(player)) {
					bar.addPlayer(player);
				}

				if (hp == 0) {
					Profile prof = main.getProfileManager().getProfile(player);
					prof.addBalance(Math.round((max / 10) * 100) / 100);

					Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
						public void run() {
							player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
									TextComponent
											.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&cBalance:&6 $"
													+ main.getProfileManager().getProfile(player).getBalance())));
						}
					}, 1);
				}

				long now = Calendar.getInstance().getTimeInMillis();
				lasthit.put(player.getName(), now);
				Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
					public void run() {
						long lh = lasthit.get(player.getName());
						if (lh == now) {
							bar.removeAll();
						}
					}
				}, 5 * 20);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onReachCheck(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Entity dmger = event.getDamager();
			Entity ent = event.getEntity();
			double dist = dmger.getLocation().distance(ent.getLocation());
			if (dist >= 4.2) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onProjectileHit(ProjectileHitEvent event) {
		if (event.getEntity().getShooter() instanceof Player) {
			Player player = (Player) event.getEntity().getShooter();
			if (event.getHitEntity() != null) {
				LivingEntity ent = (LivingEntity) event.getHitEntity();
				// double health = ent.getHealth();
				Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
					public void run() {
						double hp = ent.getHealth();
						// double damage = health - hp;
						double prog = 0;
						double max = ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
						hp = (double) Math.round((hp) * 100) / 100;
						if (hp < 0) {
							hp = 0;
						} else {

							prog = (hp / max);
						}
						BossBar bar = main.getBarManager().getBossBar(player.getName());
						bar.setTitle(barFormat.replace("%hp%", hp + "").replace("%max%", max + ""));
						bar.setProgress(prog);
						if (!bar.getPlayers().contains(player)) {
							bar.addPlayer(player);
						}

						if (hp == 0) {
							Profile prof = main.getProfileManager().getProfile(player);
							prof.addBalance(Math.round((max / 10) * 100) / 100);

							Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
								public void run() {
									player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent
											.fromLegacyText(ChatColor.translateAlternateColorCodes('&', "&cBalance:&6 $"
													+ main.getProfileManager().getProfile(player).getBalance())));
								}
							}, 1);
						}

						long now = Calendar.getInstance().getTimeInMillis();
						lasthit.put(player.getName(), now);
						Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
							public void run() {
								long lh = lasthit.get(player.getName());
								if (lh == now) {
									bar.removeAll();
								}
							}
						}, 5 * 20);
					}
				}, 1);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		LivingEntity ent = event.getEntity();
		if (ent instanceof Monster || ent.getType() == EntityType.WOLF) {
			Location loc = event.getLocation();
			Location spawn = new Location(loc.getWorld(), 0, loc.getY(), 0);
			int dist = (int) loc.distance(spawn) / 100;
			if (dist >= 1) {
				double hpIncrease = dist * 1;
				// double dmgIncrease = dist * 0.5;
				double maxhp = ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
				// double maxdmg =
				// ent.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getBaseValue();
				ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(maxhp + hpIncrease);
				// ent.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(maxdmg +
				// dmgIncrease);
				ent.setHealth(ent.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());

			}
		}
	}
}
