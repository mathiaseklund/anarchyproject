package me.mathiaseklund.anarchy.managers;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.vagdedes.mysql.database.MySQL;

public class ShopManager {

	Main main = Main.getMain();

	File f;
	FileConfiguration fc;
	File claimFile;
	FileConfiguration claimConfig;

	HashMap<Integer, ItemStack> items = new HashMap<Integer, ItemStack>(); // id, item
	HashMap<Integer, Double> prices = new HashMap<Integer, Double>(); // id, price
	HashMap<Integer, Long> expires = new HashMap<Integer, Long>(); // id, expiration time
	HashMap<Integer, Integer> sellers = new HashMap<Integer, Integer>(); // id, userId
	HashMap<String, Integer> inShop = new HashMap<String, Integer>(); // playerName, page
	HashMap<String, String> shopFilter = new HashMap<String, String>();

	NamespacedKey priceKey = new NamespacedKey(main, "price");
	NamespacedKey sellerKey = new NamespacedKey(main, "seller");
	NamespacedKey itemKey = new NamespacedKey(main, "item");

	public ShopManager() {
		load();
		checkExpiration();
	}

	void load() {
		f = new File(main.getDataFolder(), "shop.yml");
		fc = YamlConfiguration.loadConfiguration(f);
		claimFile = new File(main.getDataFolder(), "claims.yml");
		claimConfig = YamlConfiguration.loadConfiguration(claimFile);

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				ResultSet result = MySQL.query("SELECT * FROM shop");
				try {
					while (result.next()) {
						int id = result.getInt("id");
						int seller = result.getInt("seller");
						double price = result.getDouble("price");
						long expire = result.getLong("expires");

						ItemStack is = fc.getItemStack(id + "");
						items.put(id, is);
						prices.put(id, price);
						expires.put(id, expire);
						sellers.put(id, seller);

					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	void checkExpiration() {
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				long now = Calendar.getInstance().getTimeInMillis();
				@SuppressWarnings("unchecked")
				HashMap<Integer, Long> exp = (HashMap<Integer, Long>) expires.clone();

				for (int i : exp.keySet()) {
					long expire = exp.get(i);
					if (now >= expire) {
						// shop listing expired.
						int sellerId = sellers.get(i);
						items.remove(i);
						prices.remove(i);
						expires.remove(i);
						sellers.remove(i);
						List<Integer> claims = claimConfig.getIntegerList("" + sellerId);
						if (!claims.contains(i)) {
							claims.add(i);
						}
						claimConfig.set(sellerId + "", claims);
						try {
							claimConfig.save(claimFile);
						} catch (IOException e) {
							e.printStackTrace();
						}

						Connection conn = MySQL.getConnection();
						try {
							PreparedStatement remove = conn.prepareStatement("DELETE FROM shop WHERE id=?");
							remove.setInt(1, i);
							remove.execute();
							remove.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}

				checkExpiration();

			}
		}, 10 * 20);
	}

	public void sellItem(Player player, ItemStack is, double price) {
		Profile profile = main.getProfileManager().getProfile(player);

		int userId = profile.getId();
		long expire = Calendar.getInstance().getTimeInMillis() + 21600000;
		// long expire = Calendar.getInstance().getTimeInMillis() + 60000;
		Connection conn = MySQL.getConnection();
		try {
			PreparedStatement insert = conn
					.prepareStatement("INSERT INTO shop (seller, price, expires) VALUES(?, ?, ?)");
			insert.setInt(1, userId);
			insert.setDouble(2, price);
			insert.setLong(3, expire);
			insert.execute();
			ResultSet result = MySQL.query("SELECT LAST_INSERT_ID();");

			if (result.next()) {
				int id = result.getInt(1);
				items.put(id, is);
				prices.put(id, price);
				expires.put(id, expire);
				sellers.put(id, userId);
				fc.set(id + "", is);
				try {
					fc.save(f);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void openClaim(Player player) {
		Inventory inv = Bukkit.createInventory(player, 54, "Item Claim");
		Profile profile = main.getProfileManager().getProfile(player);
		int userId = profile.getId();
		List<Integer> list = claimConfig.getIntegerList(userId + "");

		if (list != null) {
			for (int i : list) {
				ItemStack is = fc.getItemStack(i + "");
				if (inv.firstEmpty() >= 0) {
					inv.addItem(is);
				} else {
					break;
				}
			}
		}
		player.openInventory(inv);
	}

	public void getClaimed(Player player) {
		Profile profile = main.getProfileManager().getProfile(player);
		int userId = profile.getId();
		List<Integer> list = claimConfig.getIntegerList(userId + "");
		List<Integer> delete = new ArrayList<Integer>();
		List<Integer> save = new ArrayList<Integer>();
		if (list != null) {
			for (int i : list) {
				ItemStack is = fc.getItemStack(i + "");
				if (player.getInventory().firstEmpty() >= 0) {
					player.getInventory().addItem(is);
					delete.add(i);
				} else {
					save.add(i);
				}
			}

			for (int i : delete) {
				fc.set(i + "", null);
			}
			try {
				fc.save(f);
				claimConfig.set(userId + "", save);
				claimConfig.save(claimFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void openShop(String filter, Player player, int p) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				int page = p;
				if (page < 0) {
					page = 0;
				}
				List<ItemStack> stacks = new ArrayList<ItemStack>();
				for (int i : items.keySet()) {
					ItemStack is = new ItemStack(items.get(i));
					if (is.getType().toString().toLowerCase().contains(filter.toLowerCase())) {
						double price = prices.get(i);
						// int sellerId = sellers.get(i);
						ItemMeta im = is.getItemMeta();
						PersistentDataContainer container = im.getPersistentDataContainer();
						// container.set(priceKey, PersistentDataType.DOUBLE, price);
						// container.set(sellerKey, PersistentDataType.INTEGER, sellerId);
						container.set(itemKey, PersistentDataType.INTEGER, i);
						List<String> lore = null;
						if (im.hasLore()) {
							lore = im.getLore();
						} else {
							lore = new ArrayList<String>();
						}

						lore.add(ChatColor.translateAlternateColorCodes('&', "&ePrice:&6 $" + price));
						lore.add(ChatColor.translateAlternateColorCodes('&', "&7Right-Click to Purchase"));

						im.setLore(lore);

						is.setItemMeta(im);
						stacks.add(is);
					}

				}
				int maxPages = stacks.size() / 45;
				if (maxPages < page) {
					page = maxPages;
				}
				Inventory inv = Bukkit.createInventory(player, 54,
						ChatColor.translateAlternateColorCodes('&', "Shop - &ePage " + (page + 1)));
				for (int i = 0; i < stacks.size(); i++) {
					int cp = i / 45;
					if (cp == page) {
						inv.addItem(stacks.get(i));
					}
				}

				ItemStack prev = new ItemStack(Material.RED_STAINED_GLASS_PANE);
				ItemStack next = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
				ItemMeta im = prev.getItemMeta();
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&cPrevious Page"));
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to go to the previous page."));
				im.setLore(lore);
				prev.setItemMeta(im);
				im = next.getItemMeta();
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&aNext Page"));
				lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to go to the next page."));
				im.setLore(lore);
				next.setItemMeta(im);
				inv.setItem(53, next);
				inv.setItem(45, prev);
				int p = page;
				Bukkit.getScheduler().runTask(main, new Runnable() {
					public void run() {
						player.openInventory(inv);
						shopFilter.put(player.getName(), filter);
						inShop.put(player.getName(), p);
					}
				});

			}
		});

	}

	public boolean isInShop(Player player) {
		return inShop.containsKey(player.getName());
	}

	public void closedShop(Player player) {
		inShop.remove(player.getName());
	}

	public int getShopPage(Player player) {
		return inShop.get(player.getName());
	}

	public void attemptPurchase(ItemStack is, Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				Profile prof = main.getProfileManager().getProfile(player);
				ItemMeta im = is.getItemMeta();
				PersistentDataContainer container = im.getPersistentDataContainer();
				int itemId = container.get(itemKey, PersistentDataType.INTEGER);
				int sellerId = sellers.get(itemId);
				if (sellerId == prof.getId()) {
					player.sendMessage(
							ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Can't purchase your own item."));
				} else {
					double price = prices.get(itemId);
					ItemStack item = items.get(itemId);
					if (player.getInventory().firstEmpty() >= 0) {
						if (prof.getBalance() >= price) {
							prof.setBalance(prof.getBalance() - price);
							items.remove(itemId);
							prices.remove(itemId);
							sellers.remove(itemId);
							expires.remove(itemId);

							player.getInventory().addItem(item);

							fc.set(itemId + "", null);
							try {
								fc.save(f);
							} catch (IOException e1) {
								e1.printStackTrace();
							}

							Profile seller = main.getProfileManager().getProfileById(sellerId);
							Connection conn = MySQL.getConnection();
							if (seller != null) {
								seller.addBalance(price);
								seller.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&eYou sold an item and earned &6$" + price));
							} else {
								try {
									PreparedStatement update = conn
											.prepareStatement("UPDATE users SET balance=balance+? WHERE id=?");
									update.setDouble(1, price);
									update.setInt(2, sellerId);
									update.execute();
									update.close();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							try {
								PreparedStatement delete = conn.prepareStatement("DELETE FROM shop WHERE id=?");
								delete.setInt(1, itemId);
								delete.execute();
								delete.close();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							openShop(shopFilter.get(player.getName()), player, inShop.get(player.getName()));
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&4ERROR:&7 Not enough balance to purchase this item."));
						}
					} else {
						player.sendMessage(
								ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Not enough inventory space."));
					}
				}
			}
		});

	}

	public void openPreviousPage(Player player) {
		int current = inShop.get(player.getName());
		String filter = shopFilter.get(player.getName());
		if (current == 0) {
			// do nothing
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Already on the first page."));
		} else {
			player.closeInventory();
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					openShop(filter, player, current - 1);
				}
			});
		}
	}

	public void openNextPage(Player player) {
		int current = inShop.get(player.getName());
		String filter = shopFilter.get(player.getName());
		player.closeInventory();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				openShop(filter, player, current + 1);
			}
		});

	}
}
