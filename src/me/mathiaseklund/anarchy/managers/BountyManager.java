package me.mathiaseklund.anarchy.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;
import me.vagdedes.mysql.database.MySQL;

public class BountyManager {

	Main main = Main.getMain();
	Util util = main.getUtil();

	HashMap<Integer, Integer> targets = new HashMap<Integer, Integer>();
	HashMap<String, Integer> targetNames = new HashMap<String, Integer>();
	HashMap<Integer, Double> bounties = new HashMap<Integer, Double>();

	HashMap<String, Integer> inMenu = new HashMap<String, Integer>();

	NamespacedKey bountyKey = new NamespacedKey(main, "bounty");

	public BountyManager() {
		load();
	}

	void load() {
		getBountiesFromDatabase();
	}

	void getBountiesFromDatabase() {
		ResultSet result = MySQL.query("SELECT * FROM bounties");
		try {
			while (result.next()) {
				int id = result.getInt("id");
				int target = result.getInt("target");
				double bounty = result.getDouble("bounty");

				targets.put(id, target);
				bounties.put(id, bounty);

				ResultSet res = MySQL.query("SELECT name FROM users WHERE id=" + target);
				if (res.next()) {
					String name = res.getString("name");
					targetNames.put(name, id);
				}
			}
			result.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void attemptAddBounty(Player player, Player target, double d) {
		Profile prof = main.getProfileManager().getProfile(player);
		if (prof.getBalance() >= d) {
			double tax = (double) Math.round((d / 10.0) * 100) / 100;
			double bounty = (double) Math.round((d - tax) * 100) / 100;
			if (prof.isPremium()) {
				bounty = (double) Math.round((d) * 100) / 100;
				tax = 0.00;
			}

			prof.setBalance(prof.getBalance() - d);
			util.message(player, "&eYou've put a bounty of &6$" + bounty + "&e on &a" + target.getName()
					+ "&e with a service fee of &6$" + tax + "&e.");
			prof = main.getProfileManager().getProfile(target);
			Connection conn = MySQL.getConnection();
			if (targetNames.containsKey(target.getName())) {
				// pre existing bounty found.
				int id = targetNames.get(target.getName());
				double oldbounty = bounties.get(id);
				double newbounty = (double) Math.round((oldbounty + bounty) * 100) / 100;
				util.broadcast("&cBounty Alert!&a " + target.getName() + "&e had his/her bounty updated to &6$"
						+ newbounty + "&e.");

				bounties.put(id, newbounty);

				try {
					PreparedStatement update = conn.prepareStatement("UPDATE bounties SET bounty=? WHERE id=?");
					update.setDouble(1, newbounty);
					update.setInt(2, id);
					update.execute();
					update.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			} else {
				util.broadcast("&cBounty Alert!&a " + target.getName() + "&e had his/her bounty updated to &6$" + bounty
						+ "&e.");
				try {
					PreparedStatement insert = conn
							.prepareStatement("INSERT INTO bounties (target, bounty) VALUES (?, ?)");
					insert.setInt(1, prof.getId());
					insert.setDouble(2, bounty);
					insert.execute();
					insert.close();

					ResultSet result = MySQL.query("SELECT id FROM bounties WHERE target=" + prof.getId());
					if (result.next()) {
						int id = result.getInt("id");
						bounties.put(id, bounty);
						targetNames.put(target.getName(), id);
						targets.put(id, prof.getId());
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} else {
			util.error(player, "You do not have enough balance.");
		}
	}

	public void openBountyMenu(Player player, int p) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				int page = p;
				if (page < 0) {
					page = 0;
				}
				List<ItemStack> stacks = new ArrayList<ItemStack>();
				for (String name : targetNames.keySet()) {
					int id = targetNames.get(name);
					double bounty = bounties.get(id);

					ItemStack is = new ItemStack(Material.PLAYER_HEAD);
					SkullMeta im = (SkullMeta) is.getItemMeta();
					im.setOwner(name);
					im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&e" + name + "'s&c Bounty"));
					PersistentDataContainer container = im.getPersistentDataContainer();
					container.set(bountyKey, PersistentDataType.INTEGER, id);
					List<String> lore = null;
					if (im.hasLore()) {
						lore = im.getLore();
					} else {
						lore = new ArrayList<String>();
					}

					lore.add(ChatColor.translateAlternateColorCodes('&', "&eBounty:&6 $" + bounty));

					im.setLore(lore);

					is.setItemMeta(im);
					stacks.add(is);

				}
				int maxPages = stacks.size() / 45;
				if (maxPages < page) {
					page = maxPages;
				}
				Inventory inv = Bukkit.createInventory(player, 54,
						ChatColor.translateAlternateColorCodes('&', "Bounties - &ePage " + (page + 1)));
				for (int i = 0; i < stacks.size(); i++) {
					int cp = i / 45;
					if (cp == page) {
						inv.addItem(stacks.get(i));
					}
				}

				ItemStack prev = new ItemStack(Material.RED_STAINED_GLASS_PANE);
				ItemStack next = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
				ItemMeta im = prev.getItemMeta();
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&cPrevious Page"));
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to go to the previous page."));
				im.setLore(lore);
				prev.setItemMeta(im);
				im = next.getItemMeta();
				im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&aNext Page"));
				lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&7Click to go to the next page."));
				im.setLore(lore);
				next.setItemMeta(im);
				inv.setItem(53, next);
				inv.setItem(45, prev);
				int p = page;
				Bukkit.getScheduler().runTask(main, new Runnable() {
					public void run() {
						player.openInventory(inv);
						inMenu.put(player.getName(), p);
					}
				});

			}
		});

	}

	public int getPage(Player player) {
		return inMenu.get(player.getName());
	}

	public void openPreviousPage(Player player) {
		int current = inMenu.get(player.getName());
		if (current == 0) {
			// do nothing
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Already on the first page."));
		} else {
			player.closeInventory();
			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {

					openBountyMenu(player, current - 1);
				}
			});
		}
	}

	public void openNextPage(Player player) {
		int current = inMenu.get(player.getName());
		player.closeInventory();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				openBountyMenu(player, current + 1);
			}
		});

	}

	public void closedMenu(Player player) {
		inMenu.remove(player.getName());
	}

	public boolean isInMenu(Player player) {
		return inMenu.containsKey(player.getName());
	}

	public boolean hasBounty(Player player) {
		return targetNames.containsKey(player.getName());
	}

	public void redeemBounty(Player killer, Player target) {
		int id = targetNames.get(target.getName());
		double bounty = bounties.get(id);
		Profile prof = main.getProfileManager().getProfile(killer);
		prof.addBalance(bounty);

		util.broadcast("&aBOUNTY COMPLETED!&a " + killer.getName() + "&e killed &c" + target.getName()
				+ "&e and redeemed the &6$" + bounty + "&e bounty.");

		targetNames.remove(target.getName());
		bounties.remove(id);
		targets.remove(id);

		Connection conn = MySQL.getConnection();
		try {
			PreparedStatement delete = conn.prepareStatement("DELETE FROM bounties WHERE id=?");
			delete.setInt(1, id);
			delete.execute();
			delete.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
