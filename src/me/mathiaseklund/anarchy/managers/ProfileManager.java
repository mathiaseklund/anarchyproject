package me.mathiaseklund.anarchy.managers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;
import me.vagdedes.mysql.database.MySQL;

public class ProfileManager {

	Main main;
	Util util;

	HashMap<String, Profile> profiles = new HashMap<String, Profile>();
	HashMap<Integer, Profile> idProfiles = new HashMap<Integer, Profile>();
	HashMap<Integer, Integer> voterewards = new HashMap<Integer, Integer>();

	HashMap<Integer, String> topListNames = new HashMap<Integer, String>();
	HashMap<Integer, Double> topListBals = new HashMap<Integer, Double>();

	public ProfileManager() {
		main = Main.getMain();
		util = main.getUtil();
		load();
	}

	void load() {
		List<String> list = main.getConfig().getStringList("voterewards");
		if (list != null) {
			for (String s : list) {
				int id = Integer.parseInt(s.split(" ")[0]);
				int lvls = Integer.parseInt(s.split(" ")[1]);
				voterewards.put(id, lvls);
			}
		}

		generateBalTop();
	}

	public void loadProfile(Player player) {
		Profile profile = new Profile(player);
		profiles.put(player.getName(), profile);

	}

	public void unloadProfile(String name) {
		profiles.remove(name);
	}

	public Profile getProfile(Player player) {
		if (!profiles.containsKey(player.getName())) {
			loadProfile(player);
		}
		return profiles.get(player.getName());
	}

	public Profile getProfile(String name) {
		return profiles.get(name);
	}

	public void setProfileId(int id, Profile profile) {
		idProfiles.put(id, profile);
	}

	public Profile getProfileById(int id) {
		return idProfiles.get(id);
	}

	public void loadPlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			loadProfile(p);
		}
	}

	public void queueVoteRewards(String name) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				ResultSet result = MySQL.query("SELECT id, premium FROM users WHERE name='" + name + "'");
				try {
					if (result.next()) {
						int id = result.getInt("id");
						boolean prem = result.getBoolean("premium");
						result.close();
						int lvls = 1;
						if (prem) {
							lvls = 2;
						}
						List<String> list = main.getConfig().getStringList("voterewards");
						if (list == null) {
							list = new ArrayList<String>();
						}

						if (voterewards.containsKey(id)) {
							int olvls = voterewards.get(id);
							list.remove(id + " " + olvls);
							lvls = lvls + olvls;
						}

						voterewards.put(id, lvls);

						list.add(id + " " + lvls);

						main.getConfig().set("voterewards", list);
						main.saveConfig();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void giveVoteRewards(Profile profile) {
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				if (voterewards.containsKey(profile.getId())) {
					int lvls = voterewards.get(profile.getId());
					profile.getPlayer().setLevel(profile.getPlayer().getLevel() + lvls);

					util.message(profile.getPlayer(),
							"&aThank your for voting!&e You've been rewarded with " + lvls + " Level(s).");

					voterewards.remove(profile.getId());

					List<String> list = main.getConfig().getStringList("voterewards");
					list.remove(profile.getId() + " " + lvls);
					main.getConfig().set("voterewards", list);
					main.saveConfig();
				}
			}
		}, 20);

	}

	void generateBalTop() {
		// Generate the top 10 list.
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				ResultSet res = MySQL.query("SELECT balance, name FROM users ORDER BY balance DESC LIMIT 10");
				try {
					int i = 1;
					while (res.next()) {
						String name = res.getString("name");
						double bal = res.getDouble("balance");
						topListNames.put(i, name);
						topListBals.put(i, bal);
						i++;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		Bukkit.getScheduler().runTaskLater(main, new Runnable() {
			public void run() {
				generateBalTop();
			}
		}, 2 * 60 * 20);
	}

	public void sendBalTop(CommandSender sender) {
		util.message(sender, "&e- Balance Top List -");
		for (int i : topListNames.keySet()) {
			String name = topListNames.get(i);
			double bal = topListBals.get(i);
			util.message(sender, "&7[&e" + i + "&7]&f " + name + ":&6 $" + bal);
		}
		util.message(sender, "&e------------------");
	}

}
