package me.mathiaseklund.anarchy.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import me.mathiaseklund.anarchy.Main;

public class ItemManager {

	Main main = Main.getMain();

	HashMap<Integer, Integer> lvlRequirement = new HashMap<Integer, Integer>();
	NamespacedKey lvlKey = new NamespacedKey(main, "level");

	public ItemManager() {
		load();
	}

	void load() {
		List<String> list = main.getConfig().getStringList("level-requirement");
		if (list == null) {
			list = new ArrayList<String>();
			list.add("1 10");
			list.add("2 13");
			list.add("3 16");
			list.add("4 19");
			list.add("5 22");
			main.getConfig().set("level-requirement", list);
			main.saveConfig();
		}
		for (String s : list) {
			int level, req;

			level = Integer.parseInt(s.split(" ")[0]);
			req = Integer.parseInt(s.split(" ")[1]);

			lvlRequirement.put(level, req);
		}
	}

	public double getDefaultAttackDamage(ItemStack is) {
		double val = 0;
		switch (is.getType()) {
		case NETHERITE_SWORD:
			val = 7;
			break;
		case DIAMOND_SWORD:
			val = 6;
			break;
		case GOLDEN_SWORD:
			val = 3;
			break;
		case IRON_SWORD:
			val = 5;
			break;
		case STONE_SWORD:
			val = 4;
			break;
		case WOODEN_SWORD:
			val = 3;
			break;
		case NETHERITE_SHOVEL:
			val = 5.5;
			break;
		case DIAMOND_SHOVEL:
			val = 4.5;
			break;
		case GOLDEN_SHOVEL:
			val = 1.5;
			break;
		case IRON_SHOVEL:
			val = 3.5;
			break;
		case STONE_SHOVEL:
			val = 2.5;
			break;
		case WOODEN_SHOVEL:
			val = 1.5;
			break;
		case NETHERITE_PICKAXE:
			val = 5;
			break;
		case DIAMOND_PICKAXE:
			val = 4;
			break;
		case GOLDEN_PICKAXE:
			val = 1;
			break;
		case IRON_PICKAXE:
			val = 3;
			break;
		case STONE_PICKAXE:
			val = 2;
			break;
		case WOODEN_PICKAXE:
			val = 1;
			break;
		case NETHERITE_AXE:
			val = 9;
			break;
		case DIAMOND_AXE:
			val = 8;
			break;
		case GOLDEN_AXE:
			val = 6;
			break;
		case IRON_AXE:
			val = 8;
			break;
		case STONE_AXE:
			val = 8;
			break;
		case WOODEN_AXE:
			val = 6;
			break;
		case NETHERITE_HOE:
			val = 3;
			break;
		case DIAMOND_HOE:
			val = 3;
			break;
		case GOLDEN_HOE:
			val = 0;
			break;
		case IRON_HOE:
			val = 0;
			break;
		case STONE_HOE:
			val = 0;
			break;
		case WOODEN_HOE:
			val = 0;
			break;
		case TRIDENT:
			val = 8;
			break;
		default:
			val = 0;
			break;
		}
		return val;
	}

	public double getDefaultArmor(ItemStack is) {
		double val = 0;
		switch (is.getType()) {
		case NETHERITE_HELMET:
			val = 3;
			break;
		case NETHERITE_CHESTPLATE:
			val = 8;
			break;
		case NETHERITE_LEGGINGS:
			val = 6;
			break;
		case NETHERITE_BOOTS:
			val = 3;
			break;
		case DIAMOND_HELMET:
			val = 3;
			break;
		case GOLDEN_HELMET:
			val = 2;
			break;
		case IRON_HELMET:
			val = 2;
			break;
		case LEATHER_HELMET:
			val = 1;
			break;
		case TURTLE_HELMET:
			val = 2;
			break;
		case CHAINMAIL_HELMET:
			val = 2;
			break;
		case DIAMOND_CHESTPLATE:
			val = 8;
			break;
		case GOLDEN_CHESTPLATE:
			val = 5;
			break;
		case IRON_CHESTPLATE:
			val = 6;
			break;
		case CHAINMAIL_CHESTPLATE:
			val = 5;
			break;
		case LEATHER_CHESTPLATE:
			val = 3;
			break;
		case DIAMOND_LEGGINGS:
			val = 6;
			break;
		case GOLDEN_LEGGINGS:
			val = 3;
			break;
		case IRON_LEGGINGS:
			val = 5;
			break;
		case CHAINMAIL_LEGGINGS:
			val = 4;
			break;
		case LEATHER_LEGGINGS:
			val = 2;
			break;
		case DIAMOND_BOOTS:
			val = 3;
			break;
		case GOLDEN_BOOTS:
			val = 1;
			break;
		case IRON_BOOTS:
			val = 2;
			break;
		case CHAINMAIL_BOOTS:
			val = 1;
			break;
		case LEATHER_BOOTS:
			val = 1;
			break;
		default:
			val = 0;
			break;
		}
		return val;
	}

	public double getDefaultToughness(ItemStack is) {
		double val = 0;
		switch (is.getType()) {
		case NETHERITE_HELMET:
			val = 3;
			break;
		case NETHERITE_CHESTPLATE:
			val = 3;
			break;
		case NETHERITE_LEGGINGS:
			val = 3;
			break;
		case NETHERITE_BOOTS:
			val = 3;
			break;
		case DIAMOND_HELMET:
			val = 2;
			break;
		case DIAMOND_CHESTPLATE:
			val = 2;
			break;
		case DIAMOND_LEGGINGS:
			val = 2;
			break;
		case DIAMOND_BOOTS:
			val = 2;
			break;
		default:
			val = 0;
			break;
		}
		return val;
	}

	public double getDefaultKnockbackResistance(ItemStack is) {
		double val = 0;
		switch (is.getType()) {
		case NETHERITE_HELMET:
			val = 1;
			break;
		case NETHERITE_CHESTPLATE:
			val = 1;
			break;
		case NETHERITE_LEGGINGS:
			val = 1;
			break;
		case NETHERITE_BOOTS:
			val = 1;
			break;
		default:
			val = 0;
			break;
		}
		return val;
	}

	public double getDefaultAttackSpeed(ItemStack is) {
		double val = 0;
		switch (is.getType()) {
		default:
			val = -2.4;
			break;
		}
		return val;
	}

	public int getLevel(ItemStack is) {
		int level = 0;
		ItemMeta im = is.getItemMeta();
		PersistentDataContainer container = im.getPersistentDataContainer();
		if (container.has(lvlKey, PersistentDataType.INTEGER)) {
			level = container.get(lvlKey, PersistentDataType.INTEGER);
		}
		return level;
	}

	public int getRequirement(int i) {
		if (lvlRequirement.containsKey(i)) {
			return lvlRequirement.get(i);
		} else {
			return 0;
		}
	}

	public ItemStack addLevel(ItemStack is) {
		int level = 0;
		ItemMeta im = is.getItemMeta();
		String type = is.getType().toString();
		PersistentDataContainer container = im.getPersistentDataContainer();
		if (container.has(lvlKey, PersistentDataType.INTEGER)) {
			level = container.get(lvlKey, PersistentDataType.INTEGER);
		}
		level++;
		container.set(lvlKey, PersistentDataType.INTEGER, level);
		if (isArmor(is)) {
			im.removeAttributeModifier(Attribute.GENERIC_ARMOR);
			im.removeAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS);
			im.removeAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE);
			double armor = getDefaultArmor(is) + (0.5 * level);
			double toughness = getDefaultToughness(is);
			double kbres = getDefaultKnockbackResistance(is);
			if (type.contains("HELMET")) {
				im.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "armor",
						armor, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HEAD));
				if (toughness > 0) {
					im.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(),
							"toughness", toughness, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HEAD));
				}
				if (kbres > 0) {
					im.addAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE,
							new AttributeModifier(UUID.randomUUID(), "kbres", kbres,
									AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HEAD));
				}
			} else if (type.contains("CHESTPLATE")) {
				im.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "armor",
						armor, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.CHEST));
				if (toughness > 0) {
					im.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(),
							"toughness", toughness, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.CHEST));
				}
				if (kbres > 0) {
					im.addAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE,
							new AttributeModifier(UUID.randomUUID(), "kbres", kbres,
									AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.CHEST));
				}
			} else if (type.contains("LEGGINGS")) {
				im.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "armor",
						armor, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.LEGS));
				if (toughness > 0) {
					im.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(),
							"toughness", toughness, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.LEGS));
				}
				if (kbres > 0) {
					im.addAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE,
							new AttributeModifier(UUID.randomUUID(), "kbres", kbres,
									AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.LEGS));
				}
			} else if (type.contains("BOOTS")) {
				im.addAttributeModifier(Attribute.GENERIC_ARMOR, new AttributeModifier(UUID.randomUUID(), "armor",
						armor, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.FEET));
				if (toughness > 0) {
					im.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, new AttributeModifier(UUID.randomUUID(),
							"toughness", toughness, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.FEET));
				}
				if (kbres > 0) {
					im.addAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE,
							new AttributeModifier(UUID.randomUUID(), "kbres", kbres,
									AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.FEET));
				}
			}

		} else {
			im.removeAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE);
			im.removeAttributeModifier(Attribute.GENERIC_ATTACK_SPEED);
			double damage = getDefaultAttackDamage(is) + (0.5 * level);
			double atkSpd = getDefaultAttackSpeed(is);
			im.addAttributeModifier(Attribute.GENERIC_ATTACK_DAMAGE, new AttributeModifier(UUID.randomUUID(), "damage",
					damage, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
			im.addAttributeModifier(Attribute.GENERIC_ATTACK_SPEED, new AttributeModifier(UUID.randomUUID(), "speed",
					atkSpd, AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HAND));
		}
		is.setItemMeta(im);
		return is;
	}

	public boolean isArmor(ItemStack is) {
		return is.getType().toString().contains("HELMET") || is.getType().toString().contains("CHESTPLATE")
				|| is.getType().toString().contains("LEGGINGS") || is.getType().toString().contains("BOOTS");
	}
}
