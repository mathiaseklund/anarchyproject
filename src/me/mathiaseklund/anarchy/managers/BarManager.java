package me.mathiaseklund.anarchy.managers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

import me.mathiaseklund.anarchy.Main;

public class BarManager {

	Main main = Main.getMain();

	HashMap<String, BossBar> bars = new HashMap<String, BossBar>();

	public BarManager() {

	}

	public BossBar getBossBar(String name) {
		if (bars.containsKey(name)) {
			return bars.get(name);
		} else {
			BossBar bar = Bukkit.createBossBar("", BarColor.WHITE, BarStyle.SOLID);
			bar.setVisible(true);
			bars.put(name, bar);
			return bar;
		}
	}

	public void clearBars() {
		for (BossBar bar : bars.values()) {
			bar.removeAll();
		}
	}
}
