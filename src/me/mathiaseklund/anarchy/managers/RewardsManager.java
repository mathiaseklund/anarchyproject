package me.mathiaseklund.anarchy.managers;

import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;

public class RewardsManager {
	Main main = Main.getMain();
	Util util = main.getUtil();

	public RewardsManager() {
		load();
	}

	void load() {
		// load all rewards.
	}

	public boolean hasClaimedDaily(Player player) {
		Profile prof = main.getProfileManager().getProfile(player);
		return prof.hasClaimedDaily();
	}
}
