package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.utils.Util;

public class ResetCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();
	boolean confirming = false;

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		if (sender.isOp()) {
			if (confirming) {
				// reset the server.
				confirming = false;
				main.setResetting(true);
			} else {
				util.message(sender,
						"&cYou are about to reset the server. If you want to proceed, please type the command again.");
				confirming = true;

				Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
					public void run() {
						if (confirming) {
							confirming = false;
							util.message(sender, "&eServer Reset request timed out.");
						}
					}
				}, 5 * 20);
			}
		}
		return false;
	}

}
