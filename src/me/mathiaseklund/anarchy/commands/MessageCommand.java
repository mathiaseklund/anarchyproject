package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.utils.Util;

public class MessageCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			String message = null;
			if (args.length >= 2) {
				Player target = Bukkit.getPlayer(args[0]);
				if (target != null) {
					for (int i = 1; i < args.length; i++) {
						if (message == null) {
							message = args[i];
						} else {
							message = message + " " + args[i];
						}
					}

					main.getProfileManager().getProfile(player).sendPM(target, message);
				} else {
					util.error(sender, "Target not found.");
				}
			}
		}
		return false;
	}

}
