package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;

public class TestCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender.isOp()) {

			if (args.length == 1) {
				String name = args[0];
				Player target = Bukkit.getPlayer(name);
				if (target != null) {
					Main.getMain().setBot(target.getName());
					Main.getMain().getUtil().message(sender, "Success");
				} else {
					Main.getMain().getUtil().message(sender, "Failure");
				}
			}

		}
		return false;
	}

}
