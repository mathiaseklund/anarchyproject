package me.mathiaseklund.anarchy.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.anarchy.Main;

public class MOTDCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender.isOp()) {
			String motd = null;
			for (String s : args) {
				if (motd == null) {
					motd = s;
				} else {
					motd = motd + " " + s;
				}
			}

			main.setMotd(motd);
			main.getUtil().message(sender, "&eMOTD Updated");
		}
		return false;
	}

}
