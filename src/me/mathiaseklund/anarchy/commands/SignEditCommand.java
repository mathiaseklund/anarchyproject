package me.mathiaseklund.anarchy.commands;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;
import net.md_5.bungee.api.ChatColor;

public class SignEditCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			Profile prof = main.getProfileManager().getProfile(player);
			if (prof.isPremium()) {
				if (args.length >= 2) {
					int line = 0;
					try {
						line = Integer.parseInt(args[0]);
						if (line > 0 && line < 5) {
							line--;

							Block block = player.getTargetBlock((Set<Material>) null, 5);
							if (block.getType().toString().contains("_SIGN")) {
								Sign sign = (Sign) block.getState();

								String text = null;
								for (int i = 1; i < args.length; i++) {
									if (text == null) {
										text = args[i];
									} else {
										text = text + " " + args[i];
									}
								}

								text = ChatColor.translateAlternateColorCodes('&', text);

								sign.setLine(line, text);
								sign.update();
							} else {
								util.error(sender, "You must look at a sign to use this command.");
							}
						} else {
							util.error(sender, "Line argument must be a number between 1 and 4..");
						}
					} catch (Exception e) {
						util.error(sender, "Line argument must be a number.");
					}

				} else {
					util.message(sender,
							"&7/signedit <line> [text] &f- Change the specified line of the sign you are looking at.");
				}
			} else {
				util.error(sender, "Only premium users can use this command. To obtain premium status: /premium");
			}
		}

		return false;
	}

}
