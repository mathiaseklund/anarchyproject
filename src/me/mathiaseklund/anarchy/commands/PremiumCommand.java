package me.mathiaseklund.anarchy.commands;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;

public class PremiumCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			Profile prof = main.getProfileManager().getProfile(player);
			if (prof.isPremium()) {
				long expires = prof.getExpiration();
				long now = Calendar.getInstance().getTimeInMillis();
				long diff = expires - now;

				long seconds = diff / 1000;
				long minutes = seconds / 60;
				long hours = minutes / 60;
				long days = hours / 24;

				String repl = "";
				if (days > 0) {
					repl = days + " Day(s)";
				} else if (hours > 0) {
					repl = hours + " Hour(s)";
				} else if (minutes > 0) {
					repl = minutes + " Minutes(s)";
				} else {
					repl = seconds + " Second(s)";
				}

				util.message(player, "&eYour premium status will expire in " + repl);
			} else {
				util.message(player,
						"&eYou can purchase premium status for $2/week and be rewarded with a plethora of QOL Improvements and some priveleges.&a Visit our shop at https://anarchyproject.tebex.io/ for more info.");
			}
		} else {
			if (args.length == 2) {
				String name = args[0];
				int days = Integer.parseInt(args[1]);
				Player player = Bukkit.getPlayer(name);
				if (player != null) {
					Profile profile = main.getProfileManager().getProfile(player);
					profile.addPremium(days);
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&aGave " + player.getName() + " " + days + " day(s) of premium status."));
				} else {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Target not found."));
				}
			} else {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"&7/premium <target> <time>&e Example: /premium mathiaseklund 7(days)"));
			}
		}
		return false;
	}

}
