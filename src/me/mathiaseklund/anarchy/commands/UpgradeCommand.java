package me.mathiaseklund.anarchy.commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.managers.ItemManager;

public class UpgradeCommand implements CommandExecutor {

	Main main = Main.getMain();
	ItemManager manager = main.getItemManager();

	ArrayList<String> confirming = new ArrayList<String>();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					ItemStack is = player.getInventory().getItemInMainHand();
					if (is != null) {
						if (is.getType() != Material.AIR) {
							int level = manager.getLevel(is);
							int req = manager.getRequirement(level + 1);
							if (main.getProfileManager().getProfile(player).isPremium()) {
								req = req - 5;
							}
							int lvl = player.getLevel();
							if (req > 0) {
								if (lvl >= req) {
									if (confirming.contains(player.getName())) {
										// go through with upgrade.
										confirming.remove(player.getName());
										player.setLevel(lvl - req);
										is = manager.addLevel(is);
										player.getInventory().setItemInMainHand(is);
										player.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"&aYou've upgraded your item to Level " + (level + 1) + "!"));
									} else {
										// ask for confirmation.
										confirming.add(player.getName());
										player.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"&eYou are about to upgrade your item to Level " + (level + 1) + " for "
														+ req + " XP Levels. To confirm please retype /upgrade."));
										Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
											public void run() {
												if (confirming.contains(player.getName())) {
													confirming.remove(player.getName());
													player.sendMessage(ChatColor.translateAlternateColorCodes('&',
															"&cUpgrade Request timed out."));
												}
											}
										}, 10 * 20);
									}
								} else {
									player.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&4ERROR:&7 You have insufficient levels. Requires " + req + "."));
								}
							} else {
								player.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&4ERROR:&7 Max Level already reached."));
							}
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&4ERROR:&7 You need to have an item in your hand to upgrade."));
						}
					} else {
						player.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&4ERROR:&7 You need to have an item in your hand to upgrade."));
					}
				} else {
					sender.sendMessage("Only players can use this command.");
				}
			}
		});

		return false;
	}

}
