package me.mathiaseklund.anarchy.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;

public class ShopCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			String filter = "";
			if (args.length > 0) {
				// TODO filter code.
				filter = args[0];
			}

			main.getShopManager().openShop(filter, player, 0);
		}
		return false;
	}

}
