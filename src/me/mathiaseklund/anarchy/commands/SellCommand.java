package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;

public class SellCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					ItemStack is = player.getInventory().getItemInMainHand();
					if (is != null) {
						if (is.getType() != Material.AIR) {
							if (args.length == 1) {
								try {
									double price = (double) Math.round(Double.parseDouble(args[0]) * 100) / 100;
									if (price > 0) {
										Profile prof = main.getProfileManager().getProfile(player);
										boolean cont = true;
										double tax = 0;
										if (!prof.isPremium()) {
											tax = price / 10.0;
											if (tax >= 0.01) {
												if (prof.getBalance() >= tax) {
													prof.setBalance(prof.getBalance() - tax);
												} else {
													cont = false;
												}
											}
										}

										if (cont) {
											player.getInventory().setItemInMainHand(null);
											main.getShopManager().sellItem(player, is, price);
											player.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&eYou've put up an item for sale for &6$" + price
															+ "&e and paid &6$" + tax + "&e Service Tax."));
										} else {
											player.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&4ERROR:&7 You do not have enough money to sell this item."));
										}

									} else {
										player.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"&4ERROR:&7 Price can't be $0."));
									}
								} catch (Exception e) {
									player.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&4ERROR:&7 Price must be a Integer or Double."));
								}
							} else {
								player.sendMessage(
										ChatColor.translateAlternateColorCodes('&', "&7Usage:&e /sell <price>"));
							}
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&4ERROR:&7 You need to hold an item in your hand that you want to sell."));
						}
					} else {
						player.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&4ERROR:&7 You need to hold an item in your hand that you want to sell."));
					}
				}
			}
		});

		return false;
	}

}
