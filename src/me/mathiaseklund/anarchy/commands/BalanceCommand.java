package me.mathiaseklund.anarchy.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;

public class BalanceCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			Profile profile = main.getProfileManager().getProfile(player);

			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cBalance:&6 $" + profile.getBalance()));
		} else {
			// console command
		}
		return false;
	}

}
