package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;

public class DailyCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					Profile prof = main.getProfileManager().getProfile(player);
					if (prof.hasClaimedDaily()) {
						util.error(sender, "You have already claimed the daily reward today. Try again tomorrow!");
					} else {
						prof.claimDaily();
					}
				}
			}
		});
		return false;
	}

}
