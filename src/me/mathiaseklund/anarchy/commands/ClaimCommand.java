package me.mathiaseklund.anarchy.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;

public class ClaimCommand implements CommandExecutor {

	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			// main.getShopManager().openClaim(player);
			main.getShopManager().getClaimed(player);
		}
		return false;
	}

}
