package me.mathiaseklund.anarchy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;

public class BountiesCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		Bukkit.getScheduler().runTaskAsynchronously(Main.getMain(), new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					Main.getMain().getBountyManager().openBountyMenu(player, 0);
				}
			}
		});
		return false;
	}

}
