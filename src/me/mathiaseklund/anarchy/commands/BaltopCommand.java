package me.mathiaseklund.anarchy.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.anarchy.Main;

public class BaltopCommand implements CommandExecutor {
	Main main = Main.getMain();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		main.getProfileManager().sendBalTop(sender);
		return false;
	}

}
