package me.mathiaseklund.anarchy.commands;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.objects.Profile;
import me.mathiaseklund.anarchy.utils.Util;

public class BountyCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	HashMap<String, String> target = new HashMap<String, String>();
	HashMap<String, Double> bounty = new HashMap<String, Double>();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					Profile prof = main.getProfileManager().getProfile(player);
					if (args.length == 0) {
						util.message(player, "&e/bounty menu&f - Open the bounty menu.");
						util.message(player, "&e/bounty add <target> <amount>&f - Add to or create a bounty.");
					} else {
						if (args[0].equalsIgnoreCase("add")) {
							if (args.length == 3) {
								String name = args[1];
								double d = 0;
								try {
									d = Double.parseDouble(args[2]);

									target.put(player.getName(), name);
									bounty.put(player.getName(), d);

									double tax = (double) Math.round((d / 10.0) * 100) / 100;
									double b = (double) Math.round((d - tax) * 100) / 100;

									if (prof.isPremium()) {
										util.message(sender, "&eYou are about to place a bounty of &6$" + d + "&e on &a"
												+ name + "&e. To confirm, type /bounty confirm.");
									} else {
										util.message(sender,
												"&eYou are about to place a bounty of &6$" + b
														+ "&e with a service fee of &6$" + tax + "&e on &a" + name
														+ "&e. To confirm, type /bounty confirm.");
									}
								} catch (Exception e) {
									util.error(sender, "Bounty amount has to be a Number.");
								}
							} else {
								util.message(player, "&e/bounty add <target> <amount>&f - Add to or create a bounty.");
							}
						} else if (args[0].equalsIgnoreCase("confirm")) {
							if (target.containsKey(player.getName())) {
								String name = target.get(player.getName());
								double d = bounty.get(player.getName());
								target.remove(player.getName());
								bounty.remove(player.getName());

								Player target = Bukkit.getPlayer(name);
								if (target != null) {

									main.getBountyManager().attemptAddBounty(player, target, d);
								} else {
									util.error(sender, "Target player not found.");
								}
							} else {
								util.error(sender, "You need to place a bounty before you can confirm.");
							}
						} else {
							main.getBountyManager().openBountyMenu(player, 0);
						}
					}
				}
			}
		});

		return false;
	}

}
