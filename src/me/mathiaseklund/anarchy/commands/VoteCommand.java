package me.mathiaseklund.anarchy.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.utils.Util;

public class VoteCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
		List<String> list = main.getConfig().getStringList("votes");
		if (list != null) {
			util.message(sender, "&eVoting Websites:");
			for (String s : list) {
				util.message(sender, "&a" + s);
				util.message(sender, "&7----------------------------");
			}
		}
		return false;
	}

}
