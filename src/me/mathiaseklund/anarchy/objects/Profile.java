package me.mathiaseklund.anarchy.objects;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.anarchy.Main;
import me.mathiaseklund.anarchy.utils.Util;
import me.vagdedes.mysql.database.MySQL;
import me.vagdedes.mysql.database.SQL;
import net.md_5.bungee.api.ChatColor;

public class Profile {

	Main main;
	Util util;
	Player player;

	double balance;
	int id, claimed_daily;
	String uuid, name;
	boolean premium;
	long premiumExpiration;

	Player lastMessager;
	Player lastMessaged;

	public Profile(Player player) {
		this.player = player;
		this.main = Main.getMain();
		this.util = main.getUtil();
		this.uuid = player.getUniqueId().toString();
		this.name = player.getName();
		load();
	}

	void load() {
		Profile profile = this;
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				ResultSet result = MySQL
						.query("SELECT id, name, balance, premium, expire, claimed_daily FROM users WHERE uuid='" + uuid
								+ "'");
				try {
					if (result.next()) {
						id = result.getInt("id");
						String n = result.getString("name");
						balance = result.getDouble("balance");
						premium = result.getBoolean("premium");
						premiumExpiration = result.getLong("expire");
						claimed_daily = result.getInt("claimed_daily");
						result.close();
						isPremium();

						if (!n.equals(name)) {
							Connection conn = MySQL.getConnection();
							PreparedStatement update = conn.prepareStatement("UPDATE users SET name=? WHERE id=?");
							update.setString(1, name);
							update.execute();
							update.close();
						}
					} else {
						result.close();
						SQL.insertData("uuid, name", "'" + uuid + "', '" + name + "'", "users");
						balance = 0;
						premium = false;
						claimed_daily = -1;
						result = MySQL.query("SELECT id FROM users WHERE uuid='" + uuid + "'");
						if (result.next()) {
							id = result.getInt("id");
						}
						result.close();
					}

					main.getProfileManager().setProfileId(id, profile);
					main.getProfileManager().giveVoteRewards(profile);
				} catch (SQLException e) {
					balance = 0;
					premium = false;
					e.printStackTrace();
				}
			}
		});

	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double d) {

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {

				balance = (double) Math.round(d * 100) / 100;
				Connection conn = MySQL.getConnection();
				PreparedStatement update;
				try {
					update = conn.prepareStatement("UPDATE users SET balance=? WHERE id=?");
					update.setDouble(1, getBalance());
					update.setInt(2, id);
					update.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});

	}

	public void addBalance(double d) {
		setBalance(getBalance() + d);
	}

	public void resetBalance() {
		if (isPremium()) {
			setBalance(getBalance() / 2);
		} else {
			setBalance(0);
		}
	}

	public boolean isPremium() {
		if (premium) {
			long expiration = getExpiration();
			long now = Calendar.getInstance().getTimeInMillis();
			if (now >= expiration) {
				setPremium(false);
				return false;
			}
		}
		return premium;
	}

	public long getExpiration() {
		return premiumExpiration;
	}

	public void setPremium(boolean b) {
		premium = b;
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				Connection conn = MySQL.getConnection();
				PreparedStatement update;
				try {
					update = conn.prepareStatement("UPDATE users SET premium=? WHERE id=?");
					update.setBoolean(1, b);
					update.setInt(2, id);
					update.execute();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void addPremium(int days) {

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				long now = Calendar.getInstance().getTimeInMillis();
				long dayms = 86400000;
				long expiration = now + (dayms * days);
				if (isPremium()) {
					expiration = getExpiration() + (dayms * days);
					Connection conn = MySQL.getConnection();
					PreparedStatement update;
					try {
						update = conn.prepareStatement("UPDATE users SET expire=? WHERE id=?");
						update.setLong(1, expiration);
						update.setInt(2, id);
						update.execute();
						premiumExpiration = expiration;
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					premiumExpiration = expiration;
					premium = true;
					Connection conn = MySQL.getConnection();
					PreparedStatement update;
					try {
						update = conn.prepareStatement("UPDATE users SET premium=?,expire=? WHERE id=?");
						update.setBoolean(1, true);
						update.setLong(2, expiration);
						update.setInt(3, id);
						update.execute();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
		});
	}

	public int getId() {
		return id;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean hasClaimedDaily() {
		boolean b = false;
		if (claimed_daily > -1) {
			Calendar calendar = Calendar.getInstance();
			int day = calendar.get(Calendar.DAY_OF_WEEK);
			if (claimed_daily == day) {
				b = true;
			}
		}
		return b;
	}

	public void claimDaily() {
		int levels = 7;
		double bal = 7000;
		if (isPremium()) {
			levels = 11;
			bal = 14000;
		}
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		player.setLevel(player.getLevel() + levels);
		addBalance(bal);
		util.message(player,
				"&eYou've claimed your daily reward and received &a" + levels + " Levels & &6$" + bal + "&e.");
		claimed_daily = day;
		Connection conn = MySQL.getConnection();
		try {
			PreparedStatement update = conn.prepareStatement("UPDATE users SET claimed_daily=? WHERE id=?");
			update.setInt(1, day);
			update.setInt(2, id);
			update.execute();
			update.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setLastMessaged(Player player) {
		lastMessaged = player;
	}

	public void setLastMessager(Player player) {
		lastMessager = player;
	}

	public Player getLastMesasged() {
		return lastMessaged;
	}

	public Player getLastMessager() {
		return lastMessager;
	}

	public void sendPM(Player target, String message) {
		if (message != null) {
			if (target != null) {
				String formatSender = ChatColor.translateAlternateColorCodes('&',
						"&7[PM]&r &eYou&r -> %target%&r: &5%message%");
				String formatTarget = ChatColor.translateAlternateColorCodes('&',
						"&7[PM]&r %sender%&r -> &eYou&r: &5%message%");

				formatSender = formatSender.replace("%target%", target.getDisplayName()).replace("%message%", message);
				formatTarget = formatTarget.replace("%sender%", player.getDisplayName()).replace("%message%", message);

				target.sendMessage(formatTarget);
				player.sendMessage(formatSender);

				Profile tProfile = main.getProfileManager().getProfile(target);
				tProfile.setLastMessager(player);
				setLastMessaged(target);
			} else {
				util.error(player, "Need a target to private message.");
			}
		} else {
			util.error(player, "Can't send an empty message.");
		}

	}

	public void sendResponse(String message) {
		if (message != null) {
			Player target = getLastMessager();
			if (target != null) {
				sendPM(target, message);
			} else {
				util.error(player, "No player to respond to.");
			}
		} else {
			util.error(player, "Can't send an empty message.");
		}
	}
}
