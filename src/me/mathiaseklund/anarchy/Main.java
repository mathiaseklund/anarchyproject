package me.mathiaseklund.anarchy;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.anarchy.commands.BalanceCommand;
import me.mathiaseklund.anarchy.commands.BaltopCommand;
import me.mathiaseklund.anarchy.commands.BountiesCommand;
import me.mathiaseklund.anarchy.commands.BountyCommand;
import me.mathiaseklund.anarchy.commands.ClaimCommand;
import me.mathiaseklund.anarchy.commands.DailyCommand;
import me.mathiaseklund.anarchy.commands.MOTDCommand;
import me.mathiaseklund.anarchy.commands.MessageCommand;
import me.mathiaseklund.anarchy.commands.PremiumCommand;
import me.mathiaseklund.anarchy.commands.ResetCommand;
import me.mathiaseklund.anarchy.commands.RespondCommand;
import me.mathiaseklund.anarchy.commands.SellCommand;
import me.mathiaseklund.anarchy.commands.ShopCommand;
import me.mathiaseklund.anarchy.commands.SignEditCommand;
import me.mathiaseklund.anarchy.commands.SuicideCommand;
import me.mathiaseklund.anarchy.commands.TestCommand;
import me.mathiaseklund.anarchy.commands.TrashCommand;
import me.mathiaseklund.anarchy.commands.UpgradeCommand;
import me.mathiaseklund.anarchy.commands.VoteCommand;
import me.mathiaseklund.anarchy.listeners.EntityListener;
import me.mathiaseklund.anarchy.listeners.InventoryListener;
import me.mathiaseklund.anarchy.listeners.ItemListener;
import me.mathiaseklund.anarchy.listeners.PlayerListener;
import me.mathiaseklund.anarchy.listeners.ServerListener;
import me.mathiaseklund.anarchy.listeners.VoteListener;
import me.mathiaseklund.anarchy.managers.BarManager;
import me.mathiaseklund.anarchy.managers.BountyManager;
import me.mathiaseklund.anarchy.managers.ItemManager;
import me.mathiaseklund.anarchy.managers.ProfileManager;
import me.mathiaseklund.anarchy.managers.QuestManager;
import me.mathiaseklund.anarchy.managers.RewardsManager;
import me.mathiaseklund.anarchy.managers.ShopManager;
import me.mathiaseklund.anarchy.tabcompletion.SignEditTabComplete;
import me.mathiaseklund.anarchy.utils.Util;
import me.vagdedes.mysql.database.MySQL;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {

	static Main main;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		loadFiles();
		loadDependencies();
		loadManagers();
		loadListeners();
		loadCommands();

		getProfileManager().loadPlayers();

	}

	public void onDisable() {
		bm.clearBars();
	}

	ProfileManager pm;
	ShopManager sm;
	BarManager bm;
	ItemManager im;
	Util util;
	BountyManager bom;
	QuestManager qm;
	RewardsManager rm;

	boolean resetting = false;
	String motd;

	ArrayList<String> bots = new ArrayList<String>();

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}

		f = new File(getDataFolder(), "shop.yml");
		if (!f.exists()) {
			saveResource("shop.yml", true);
		}
	}

	void loadListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(), main);
		getServer().getPluginManager().registerEvents(new InventoryListener(), main);
		getServer().getPluginManager().registerEvents(new EntityListener(), main);
		getServer().getPluginManager().registerEvents(new ItemListener(), main);
		getServer().getPluginManager().registerEvents(new VoteListener(), main);
		getServer().getPluginManager().registerEvents(new ServerListener(), main);
	}

	void loadCommands() {
		getCommand("balance").setExecutor(new BalanceCommand());
		getCommand("motd").setExecutor(new MOTDCommand());
		getCommand("premium").setExecutor(new PremiumCommand());
		getCommand("shop").setExecutor(new ShopCommand());
		getCommand("sell").setExecutor(new SellCommand());
		getCommand("claim").setExecutor(new ClaimCommand());
		getCommand("test").setExecutor(new TestCommand());
		getCommand("upgrade").setExecutor(new UpgradeCommand());
		getCommand("bounty").setExecutor(new BountyCommand());
		getCommand("bounties").setExecutor(new BountiesCommand());
		getCommand("daily").setExecutor(new DailyCommand());
		getCommand("reset").setExecutor(new ResetCommand());
		getCommand("signedit").setExecutor(new SignEditCommand());
		getCommand("signedit").setTabCompleter(new SignEditTabComplete());
		getCommand("vote").setExecutor(new VoteCommand());
		getCommand("suicide").setExecutor(new SuicideCommand());
		getCommand("baltop").setExecutor(new BaltopCommand());
		getCommand("trash").setExecutor(new TrashCommand());
		getCommand("message").setExecutor(new MessageCommand());
		getCommand("respond").setExecutor(new RespondCommand());
		getCommand("enderchest").setExecutor(new EnderChestCommand());
	}

	void loadDependencies() {
		/*
		 * for (World w : Bukkit.getWorlds()) { if (w.getName().contains("nether")) {
		 * w.getWorldBorder().setSize(8000); } else if (w.getName().contains("the_end"))
		 * { w.getWorldBorder().setSize(8000); } else {
		 * w.getWorldBorder().setSize(8000); } }
		 */
	}

	void loadManagers() {
		util = new Util();
		pm = new ProfileManager();
		sm = new ShopManager();
		bm = new BarManager();
		im = new ItemManager();
		bom = new BountyManager();
		qm = new QuestManager();
		rm = new RewardsManager();
	}

	public ProfileManager getProfileManager() {
		return pm;
	}

	public ShopManager getShopManager() {
		return sm;
	}

	public BarManager getBarManager() {
		return bm;
	}

	public ItemManager getItemManager() {
		return im;
	}

	public Util getUtil() {
		return util;
	}

	public BountyManager getBountyManager() {
		return bom;
	}

	public QuestManager getQuestManager() {
		return qm;
	}

	public RewardsManager getRewardsManager() {
		return rm;
	}

	public boolean isResetting() {
		return resetting;
	}

	public void setResetting(boolean b) {
		resetting = b;

		if (b) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.kickPlayer(ChatColor.translateAlternateColorCodes('&',
						"&aThanks for playing this week of the Anarchy Project! Server is now resetting and getting ready for the new week."));
			}

			File f = new File(getDataFolder(), "shop.yml");
			if (f.exists()) {
				f.delete();
			}
			f = new File(getDataFolder(), "claims.yml");
			if (f.exists()) {
				f.delete();
			}

			ResultSet result = MySQL.query("SELECT * FROM users ORDER BY balance DESC");
			Connection conn = MySQL.getConnection();
			int c = 0;
			long now = Calendar.getInstance().getTimeInMillis();
			try {
				while (result.next()) {
					boolean first = false;
					boolean second = false;
					boolean third = false;
					if (c == 0) {
						first = true;
						c++;
					} else if (c == 1) {
						second = true;
						c++;
					} else if (c == 2) {
						third = true;
						c++;
					}

					int id = result.getInt("id");
					String uuid = result.getString("uuid");
					String name = result.getString("name");
					double balance = result.getDouble("balance");
					boolean premium = result.getBoolean("premium");
					long expires = result.getLong("expire");

					if (first) {
						if (premium) {
							expires = expires + (86400000 * 7);
						} else {
							premium = true;
							expires = now + (86400000 * 7);
						}

						PreparedStatement insert = conn.prepareStatement(
								"INSERT INTO winners (userid, uuid, name, balance, place) VALUES (?, ?, ?, ?, 1)");
						insert.setInt(1, id);
						insert.setString(2, uuid);
						insert.setString(3, name);
						insert.setDouble(4, balance);
						insert.execute();
						insert.close();
					} else if (second) {
						if (premium) {
							expires = expires + (86400000 * 3);
						} else {
							premium = true;
							expires = now + (86400000 * 3);
						}

						PreparedStatement insert = conn.prepareStatement(
								"INSERT INTO winners (userid, uuid, name, balance, place) VALUES (?, ?, ?, ?, 2)");
						insert.setInt(1, id);
						insert.setString(2, uuid);
						insert.setString(3, name);
						insert.setDouble(4, balance);
						insert.execute();
						insert.close();
					} else if (third) {
						if (premium) {
							expires = expires + (86400000 * 1);
						} else {
							premium = true;
							expires = now + (86400000 * 1);
						}

						PreparedStatement insert = conn.prepareStatement(
								"INSERT INTO winners (userid, uuid, name, balance, place) VALUES (?, ?, ?, ?, 3)");
						insert.setInt(1, id);
						insert.setString(2, uuid);
						insert.setString(3, name);
						insert.setDouble(4, balance);
						insert.execute();
						insert.close();
					}

					PreparedStatement update = conn.prepareStatement(
							"UPDATE users SET balance=0, premium=?, expire=?, claimed_daily=-1 WHERE id=?");
					update.setBoolean(1, premium);
					update.setLong(2, expires);
					update.setInt(3, id);
					update.execute();
					update.close();

					PreparedStatement truncate = conn.prepareStatement("TRUNCATE TABLE bounties");
					truncate.execute();
					truncate.close();
					truncate = conn.prepareStatement("TRUNCATE TABLE shop");
					truncate.execute();
					truncate.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			File wf = Bukkit.getWorldContainer();
			World w = Bukkit.getWorld("world");
			Bukkit.unloadWorld(w, false);
			w = Bukkit.getWorld("world_nether");
			Bukkit.unloadWorld(w, false);
			w = Bukkit.getWorld("world_the_end");
			Bukkit.unloadWorld(w, false);
			f = new File(wf.getPath() + "/world/");
			if (f.exists()) {
				try {
					FileUtils.deleteDirectory(f);
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
			f = new File(wf.getPath() + "/world_nether/");
			if (f.exists()) {
				try {
					FileUtils.deleteDirectory(f);
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
			f = new File(wf.getPath() + "/world_the_end/");
			if (f.exists()) {
				try {
					FileUtils.deleteDirectory(f);
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}

			long seed = 0;
			Random rand = new Random();
			seed = rand.nextInt(999999999);

			WorldCreator wc = new WorldCreator("world");
			wc.environment(World.Environment.NORMAL);
			wc.type(WorldType.NORMAL);
			wc.seed(seed);

			wc.createWorld();

			wc = new WorldCreator("world_nether");
			wc.environment(World.Environment.NETHER);
			wc.type(WorldType.NORMAL);
			wc.seed(seed);

			wc.createWorld();

			wc = new WorldCreator("world_the_end");
			wc.environment(World.Environment.THE_END);
			wc.type(WorldType.NORMAL);
			wc.seed(seed);

			wc.createWorld();

			getServer().dispatchCommand(Bukkit.getConsoleSender(), "stop");
		}
	}

	public String getMotd() {
		if (motd == null) {
			motd = getConfig().getString("motd");
		}
		return motd;
	}

	public void setMotd(String motd) {
		this.motd = motd;
		getConfig().set("motd", motd);
		saveConfig();
	}

	public void setBot(String bot) {
		if (!bots.contains(bot)) {
			bots.add(bot);
		}
	}

	public boolean isBot(String name) {
		return bots.contains(name);
	}
}
