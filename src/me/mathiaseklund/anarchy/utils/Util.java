package me.mathiaseklund.anarchy.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.anarchy.Main;

public class Util {

	Main main = Main.getMain();

	List<String> bcList;
	int c = 0;

	public Util() {

		broadcastMessages();
	}

	void broadcastMessages() {
		if (bcList == null) {
			bcList = main.getConfig().getStringList("broadcasts");
		}
		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				if (bcList.size() > c) {

				} else {
					c = 0;
				}

				String msg = bcList.get(c);

				broadcast(msg);
				c++;
				broadcastMessages();

			}
		}, 2 * 60 * 20);
	}

	public void error(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				message(sender, "&4ERROR:&7 " + message);
			}
		}
	}

	public void message(CommandSender sender, String message) {
		if (sender != null) {
			if (message != null) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}
	}

	public void broadcast(String message) {
		if (message != null) {
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', ""));
			Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}
}
